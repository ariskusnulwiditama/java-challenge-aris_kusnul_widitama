
CREATE TABLE public.user (
    "id" bigserial NOT NULL,

    "username" varchar(25) NOT null unique,
 
    "password" varchar(25) NOT NULL,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);
