package ist.challenge.aris_kusnul_widitama.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResourceAlreadyExistException extends Throwable{
    public static ResponseEntity<Object> existsException(HttpStatus status, Object responseObj) {

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("status", status.value());
        map.put("data", responseObj);

        return new ResponseEntity<Object>(map,status);
    }
}


