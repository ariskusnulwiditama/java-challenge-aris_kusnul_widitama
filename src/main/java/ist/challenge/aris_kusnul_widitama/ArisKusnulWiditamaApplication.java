package ist.challenge.aris_kusnul_widitama;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArisKusnulWiditamaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArisKusnulWiditamaApplication.class, args);
	}

}
