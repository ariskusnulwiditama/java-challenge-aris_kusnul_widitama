package ist.challenge.aris_kusnul_widitama.dtos;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponseDTO {
    private Long id;
    private String username;

    @Override
    public String toString() {
        return "UserResponseDTO{" +
                "userId=" + this.id +
                ", username='" + this.username + '\'' +
                '}';
    }
}


