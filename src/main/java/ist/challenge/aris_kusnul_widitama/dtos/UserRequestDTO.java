package ist.challenge.aris_kusnul_widitama.dtos;

import ist.challenge.aris_kusnul_widitama.models.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class UserRequestDTO {
    private Long id;

    private String username;
    
    private String password;

 
    public User convertToEntity(){
        return User.builder()
            .id(this.id)
            .username(this.username)
            .password(this.password)
            .build();
    }

    /***
     * request to update existing User by ID
     * @return
     */
    public User updateEntity(){
        return User.builder()
            .username(this.username)
            .password(this.password)
            .build();
    }


}