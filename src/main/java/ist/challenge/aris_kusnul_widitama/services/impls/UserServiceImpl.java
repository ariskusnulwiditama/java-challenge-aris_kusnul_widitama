package ist.challenge.aris_kusnul_widitama.services.impls;

import ist.challenge.aris_kusnul_widitama.dtos.UserRequestDTO;
import ist.challenge.aris_kusnul_widitama.dtos.UserResponseDTO;
import ist.challenge.aris_kusnul_widitama.exceptions.ResourceAlreadyExistException;
import ist.challenge.aris_kusnul_widitama.exceptions.ResourceNotFoundException;
import ist.challenge.aris_kusnul_widitama.handler.ResponseHandler;
import ist.challenge.aris_kusnul_widitama.models.User;
import ist.challenge.aris_kusnul_widitama.repositories.UserRepository;
import ist.challenge.aris_kusnul_widitama.services.UserService;
import lombok.AllArgsConstructor;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor


public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;


    @Override
    public ResponseEntity<Object> register(UserRequestDTO userRequestDTO) {
        try{
            if(userRequestDTO.getUsername().isEmpty() || userRequestDTO.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Please Complete All Field!!");
            }
            if(userRepository.existsByUsername(userRequestDTO.getUsername())){
                return ResourceAlreadyExistException.existsException(HttpStatus.CONFLICT, "Username already taken!!");
            }
            User user = new User();
            user.setUsername(userRequestDTO.getUsername());
            user.setPassword(userRequestDTO.getPassword());
            userRepository.save(user);
            UserResponseDTO userResponseDTO = user.convertToResponse();
            return ResponseHandler.generateResponse("Create User Successfully", HttpStatus.CREATED, userResponseDTO);
        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Failed Create User");
        }
    }

    @Override
    public ResponseEntity<Object> login(UserRequestDTO userRequestDTO) {
        try{
            if(userRequestDTO.getUsername().isEmpty() || userRequestDTO.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Username and / or Password is Empty!!");
            }
            if(!userRepository.existsByUsername(userRequestDTO.getUsername())){
                return ResourceAlreadyExistException.existsException(HttpStatus.FORBIDDEN, "Username Doesn't Exist!!");
            }
            Optional<User> user = userRepository.findByUsername(userRequestDTO.getUsername());
            if(!userRequestDTO.getPassword().equals(user.get().getPassword())){
                return ResourceAlreadyExistException.existsException(HttpStatus.FORBIDDEN, "Password is wrong!!");
            }
            return ResponseHandler.successResponse("Successfully login!", HttpStatus.OK);
        }catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Login Failed!!");
        }
    }

    @Override
    public ResponseEntity<Object> updateUser(Long id, UserRequestDTO userRequestDTO) {
        try{
            if(userRequestDTO.getUsername().isEmpty() || userRequestDTO.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Username and / or password is empty!!");
            }
            User user = userRepository.getById(id);
            if(userRepository.existsByUsername(userRequestDTO.getUsername())){
                return ResourceAlreadyExistException.existsException(HttpStatus.CONFLICT, "Username already taken!!");
            }

            if(userRequestDTO.getPassword().equals(user.getPassword())){
                return ResourceAlreadyExistException.existsException(HttpStatus.BAD_REQUEST, "Password cannot be the same as the previous password!!");
            }
            user.setId(id);
            user.setUsername(userRequestDTO.getUsername());
            user.setPassword(userRequestDTO.getPassword());
            userRepository.save(user);
            UserResponseDTO userResponseDTO = user.convertToResponse();
            return ResponseHandler.generateResponse("Update Data Successfully!!", HttpStatus.CREATED,userResponseDTO);
        }catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Failed Update Data!!");
        }
    }


    @Override
    public ResponseEntity<Object> getAllUsers() {
        try {
            List<User> user = userRepository.findAll();
            if(user.isEmpty()){
                throw new ResourceNotFoundException("Data Not Found!!");
            }
            List<UserResponseDTO> userResponseList = new ArrayList<>();
            for (User dataResult : user){
                UserResponseDTO userResponseDTO = dataResult.convertToResponse();
                userResponseList.add(userResponseDTO);
            }
            return ResponseHandler.generateResponse("Retrieved Data Successfully!!", HttpStatus.OK, userResponseList);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Bad Request");
        }
    }

}

