package ist.challenge.aris_kusnul_widitama.services;

import ist.challenge.aris_kusnul_widitama.dtos.UserRequestDTO;


import org.springframework.http.ResponseEntity;



public interface UserService {
    ResponseEntity<Object> getAllUsers();

    ResponseEntity<Object> register(UserRequestDTO userRequestDTO);

    ResponseEntity<Object> login(UserRequestDTO userRequestDTO);

    ResponseEntity<Object> updateUser(Long id, UserRequestDTO userRequestDTO);


}
