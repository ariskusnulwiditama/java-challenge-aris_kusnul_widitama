package ist.challenge.aris_kusnul_widitama.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import ist.challenge.aris_kusnul_widitama.dtos.UserRequestDTO;

import ist.challenge.aris_kusnul_widitama.services.UserService;
import lombok.AllArgsConstructor;

import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

@Tag(name = "User Controller")
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {
    UserService userService;

    @PostMapping("/signUp")
    public ResponseEntity<Object> register(@RequestBody UserRequestDTO userRequestDTO){
        return userService.register(userRequestDTO);
    }

    @PostMapping("/signIn")
    public ResponseEntity<Object> loginUser(@RequestBody UserRequestDTO userRequestDTO){
        return userService.login(userRequestDTO);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody UserRequestDTO userRequestDTO){
        return userService.updateUser(id, userRequestDTO);
    }

    @GetMapping("/listUser")
    public ResponseEntity <Object> getAllUser(){
        return userService.getAllUsers();
    }

}


