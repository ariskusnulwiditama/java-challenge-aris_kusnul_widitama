package ist.challenge.aris_kusnul_widitama.models;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;

import ist.challenge.aris_kusnul_widitama.dtos.UserResponseDTO;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "user", schema = "public")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;

    public UserResponseDTO convertToResponse(){
        return UserResponseDTO.builder()
                .id(this.id)
                .username(this.username)
                .build();
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + this.id +
                ", username='" + this.username + '\'' +
                ", password='" + this.password + '\'' +
                '}';
    }
}
